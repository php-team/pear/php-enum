php-enum (4.7.0-1) unstable; urgency=medium

  * Upload to unstable for latest php-json-schema

  [ Jérémy DECOOL ]
  * Implement Stringable interface on Enum

  [ David Prévot ]
  * Update Standards-Version to 4.7.0
  * Mark Data Provider methods as static (Closes: #1039775, #1070541)

 -- David Prévot <taffit@debian.org>  Thu, 25 Jul 2024 23:12:15 +0900

php-enum (4.6.1-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * fix error on `clone $enum`

  [ David Prévot ]
  * Build-conflicts php-symfony-polyfill-mbstring that get pulled instead of
    php-mbstring by buildd in experimental
  * Mark package as Multi-Arch: foreign

 -- David Prévot <taffit@debian.org>  Wed, 16 Feb 2022 12:55:48 -0400

php-enum (4.6.0-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * Upgrade dev dependencies

 -- David Prévot <taffit@debian.org>  Sat, 08 Jan 2022 21:01:15 -0400

php-enum (4.6.0~rc1-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * Simplified .gitignore & .gitattributes
  * Test: run on PHP-8 stable
  * Support for PHP 8.1

  [ David Prévot ]
  * Update standards version to 4.6.0, no changes needed.
  * Fix autoloaders file for pkg-php-tools

 -- David Prévot <taffit@debian.org>  Tue, 12 Oct 2021 10:01:23 -0400

php-enum (4.4.0-1) experimental; urgency=medium

  [ Marco Pivetta ]
  * Upgraded to `phpunit/phpunit` 7.x (Closes: #980565)

  [ Marc Bennewitz ]
  * Copyright (c) 2020
  * Require php ^7.1 or ^8.0

  [ David Prévot ]
  * Document gbp import-orig usage
  * Update watch file format version to 4.
  * Set Rules-Requires-Root: no.
  * Update standards version to 4.5.1, no changes needed.
  * Use dh-sequence-phpcomposer instead of pkg-php-tools
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test
  * Update copyright (years)
  * Install /u/s/p/autoloaders file
  * Install /u/s/p/overrides file

 -- David Prévot <taffit@debian.org>  Mon, 15 Feb 2021 17:35:28 -0400

php-enum (4.2.0-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * fixed check of ambiguous values on Enum::byName()
  * PHP 7.4 serializer and wrap old Serializable methods for BC
  * fixed TypeError in EnumSet::getBinaryBitsetLe() if EnumSet is using integer
    bitset internally

  [ David Prévot ]
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

 -- David Prévot <taffit@debian.org>  Sun, 12 Jan 2020 17:56:33 -1000

php-enum (4.1.1-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * fixed ordinal cache usage of Enum::getName() if ordinal=0

  [ David Prévot ]
  * Update standards version, no changes needed.
  * Compatibility with recent PHPUnit (8)

 -- David Prévot <taffit@debian.org>  Tue, 24 Sep 2019 15:37:56 -1000

php-enum (4.1.0-1) experimental; urgency=medium

  [ Lctrs ]
  * Add EnumSet::isEmpty() to know if sets has enumerators

 -- David Prévot <taffit@debian.org>  Fri, 05 Jul 2019 16:57:25 -1000

php-enum (4.0.0-1) experimental; urgency=medium

  [ Marc Bennewitz ]
  * Copyright (c) 2019

  [ David Prévot ]
  * Use debhelper-compat 12
  * Update Standards-Version to 4.3.0
  * Update copyright (years)
  * Drop get-orig-source target

 -- David Prévot <taffit@debian.org>  Thu, 27 Jun 2019 11:38:02 -1000

php-enum (3.0.2-1) experimental; urgency=medium

  * Use debhelper-compat = 11
  * Update Standards-Version to 4.2.1

 -- David Prévot <taffit@debian.org>  Sun, 16 Sep 2018 15:54:21 -1000

php-enum (3.0.1-1) experimental; urgency=medium

  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.5

 -- David Prévot <taffit@debian.org>  Sun, 22 Jul 2018 19:41:50 +0800

php-enum (3.0.0-1) experimental; urgency=medium

  * Upload to experimental since the next version of php-json-schema should
    depend on version 2.3.1 for the time being

  [ Marc Bennewitz ]
  * v3.0 preparation
  * php --define zend.assertions=1 /usr/bin/phpunit (Closes: #882928)

  [ David Prévot ]
  * Update copyright (years)
  * Update Standards-Version to 4.1.2
  * Enable zend.assertions for the tests

 -- David Prévot <taffit@debian.org>  Sun, 03 Dec 2017 14:33:28 -1000

php-enum (2.3.1-1) unstable; urgency=low

  * Initial release (New php-json-schema dependency)

 -- David Prévot <taffit@debian.org>  Tue, 24 Oct 2017 16:35:26 -1000
